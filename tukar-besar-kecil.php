<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ubah Huruf</title>
</head>

<body>
    <?php
    echo "<h1>Tukar Besar Kecil</h1>";
    function tukar_besar_kecil($string)
    {
        //kode di sini
        $tampung = "";
        for ($i = strlen($string) - 1; $i >= 0; $i--) {
            if (ctype_upper($string[$i]) == true) {
                $tampung = strtolower($string[$i]) . $tampung;
            } else {
                $tampung = strtoupper($string[$i]) . $tampung;
            }
        }
        return $tampung;
    }

    // TEST CASES
    echo tukar_besar_kecil('Hello World <br>'); // "hELLO wORLD"
    echo tukar_besar_kecil('I aM aLAY <br>'); // "i Am Alay"
    echo tukar_besar_kecil('My Name is Bond!! <br>'); // "mY nAME IS bOND!!"
    echo tukar_besar_kecil('IT sHOULD bE me <br>'); // "it Should Be ME"
    echo tukar_besar_kecil('001-A-3-5TrdYW <br>'); // "001-a-3-5tRDyw"


    ?>
</body>

</html>