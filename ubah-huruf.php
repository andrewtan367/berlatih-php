<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ubah Huruf</title>
</head>

<body>
    <?php
    echo "<h1>Ubah Huruf</h1>";

    function ubah_huruf($string)
    {
        $tampung = "";
        for ($i = strlen($string) - 1; $i >= 0; $i--) {
            $ubah = $string[$i];
            $ubah++;
            $tampung = $ubah . $tampung;
        }
        return $tampung;
    }

    // TEST CASES
    echo ubah_huruf('wow'); // xpx
    echo "<br>";
    echo ubah_huruf('developer'); // efwfmpqfs
    echo "<br>";
    echo ubah_huruf('laravel'); // mbsbwfm
    echo "<br>";
    echo ubah_huruf('keren'); // lfsfo
    echo "<br>";
    echo ubah_huruf('semangat'); // tfnbohbu

    ?>
</body>

</html>